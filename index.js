var path = require('path');
var fs = require('fs');
var readline = require('readline');
var stream = require('stream');
var userName = process.env['USERPROFILE'].split(path.sep)[2];
var sqlFormatter = require('sql-formatter');

/*
console.log('\x1b[32m%s\x1b[0m', `
###  ###      ###  ###      ###  #########  #######      ######    #######     ########
###  ######   ###  ######   ###     ###     ###  ###    ###  ###   ###  ###    ###
###  ### ###  ###  ### ###  ###     ###     ###  ###   ###    ###  ###    ###  ###
###  ###  ### ###  ###  ### ###     ###     #######    ##########  ###    ###  ########
###  ###  ### ###  ###  ### ###     ###     ###  ###   ###    ###  ###    ###  ###
###  ###   ######  ###   ######     ###     ###   ###  ###    ###  ###  ###    ###
###  ###      ###  ###      ###     ###     ###   ###  ###    ###  #######     ########
                                                                    \xA9 JANUSZ ERAZMUS
`);
*/

console.log('\x1b[32m%s\x1b[0m', "MySQL Snippets-backup maker started!");

var snippet_file = "C:\\Users\\" + userName + "\\AppData\\Roaming\\MySQL\\Workbench\\snippets\\User Snippets.txt";

console.log('\x1b[32m%s\x1b[0m', "Searching for file: " + snippet_file);

var instream = fs.createReadStream(snippet_file);
var outstream = new stream;
var rl = readline.createInterface(instream, outstream);

class Snippet {	
	constructor(title, sql) {
		this.title = title;
		this.sql = sql;
	}
}

var all_snippets = [];
var current_snippet;
var first_done = false;

rl.on('line', function(line) {
	if (!line.startsWith(" ") && line.replace(/\s/g, "") != "") {
		if (first_done) {
			all_snippets.push(current_snippet);
		}
		else {
			console.log('\x1b[32m%s\x1b[0m', "Reading snippets...");
			first_done = !first_done;
		}
		
		current_snippet = new Snippet(line, "");
	}
	else {
		current_snippet.sql = current_snippet.sql.concat(line + '\n');
	}
});

rl.on('close', function() {
	// Push last snippet
	all_snippets.push(current_snippet);
	
	var time_stamp = Date.now();
	var dir = "./backup_" + time_stamp + "/";

	if (!fs.existsSync(dir)){
		fs.mkdirSync(dir);
	}
	for(i = 0; i < all_snippets.length; i++) {
		fs.writeFile(dir + all_snippets[i].title.replace(/[\\/:"*?<>|]/g, "_") + ".sql", sqlFormatter.format(all_snippets[i].sql), function(err) {
			if(err) {
				return console.log('\x1b[31m%s\x1b[0m', err);
			}
		});
	}
	
	console.log('\x1b[32m%s\x1b[0m', "Total of " + all_snippets.length + " snippets saved to directory: " + dir);
	
	console.log('\x1b[32m%s\x1b[0m', 'Press any key to exit');

	process.stdin.setRawMode(true);
	process.stdin.resume();
	process.stdin.on('data', process.exit.bind(process, 0));
});